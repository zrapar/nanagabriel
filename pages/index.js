import React, { useEffect } from 'react';
import Head from 'next/head';

import { i18n, withTranslation } from '../i18n';

// import Custom Components
import Header from '../containers/common/header';
import BannerSection from './sections/banner';
import SideSection from './sections/sideSection';
import AlbumSection from './sections/album';
import ArtistSection from './sections/artist';
import SponsorSection from './sections/sponsor';
import GallerySection from './sections/gallery';
import VideoSection from './sections/video';
import BookingSection from './sections/booking';
import BlogSection from './sections/blog';
import SubscribeSection from './sections/subscribe';
import TestimonialSection from './sections/testimonial';
import FooterSection from './sections/footer';
import CopyrightSection from './sections/copyright';

const Home = ({ t }) => {
	useEffect(() => {
		document.body.style.setProperty('--primary', '#223b7b');
		document.body.style.setProperty('--secondary', '#fff');
		document.body.style.setProperty('--light', '#2245a0');
		document.body.style.setProperty('--dark', '#213672');
	});

	return (
		<div>
			<Head>
				<title>Nana & Gabriel | {t('welcome')} </title>
			</Head>

			<Header className='music loding-header' shop={true} />

			<BannerSection />

			{/* <SideSection /> */}

			{/* <AlbumSection /> */}

			<ArtistSection />

			<SponsorSection />

			<GallerySection />

			<VideoSection />

			<BookingSection />

			<BlogSection />

			<SubscribeSection />

			<TestimonialSection />

			<FooterSection />

			<CopyrightSection />
		</div>
	);
};

Home.getInitialProps = async () => ({
	namespacesRequired : [ 'common' ]
});

export default withTranslation('common')(Home);
