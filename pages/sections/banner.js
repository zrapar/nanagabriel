import React, { useState } from 'react';
import Tilt from 'react-parallax-tilt';
import { Container, Row, Col } from 'reactstrap';
import { withTranslation } from '../../i18n';
import { useWindowDimensions } from '../../hooks';
import { checkIfAlive } from '../../utils';

const Banner = ({ t }) => {
  const { height, width } = useWindowDimensions();
  const [sideSection, setSideSection] = useState(false);
  const [show, toggleShow] = React.useState(false);

  React.useEffect(() => {
    checkIfAlive().then(({ alive }) => {
      toggleShow(alive);
    });
  }, []);

  const sidesection = () => {
    if (!sideSection) {
      setSideSection(true);
      document.querySelector('.side-section').classList.add('d-block');
    } else {
      setSideSection(false);
      document.querySelector('.side-section').classList.remove('d-block');
    }
  };

  return (
    <section className='music header' id='header'>
      <div className='music-content'>
        <div className='music-bg bg bg-shadow-top'>
          {/* <Tilt perspective='20000' transitionSpeed='3000'> */}
          <div className='text-center w-100'>
            <div className='img-height'>
              {show ? (
                <iframe
                  src='https://player.twitch.tv/?channel=nanagabriel&parent=192.168.1.105:3000&muted=true'
                  height={height}
                  width={width}
                  frameBorder='0'
                  scrolling='no'
                  allowFullScreen={true}
                />
              ) : (
                  <img alt='' className='img-fluid' src='/assets/images/music/header.jpg' />
                )}
            </div>
          </div>

          {/* </Tilt> */}
        </div>
      </div>

      <div className='left-side'>
        <h6 className='follow-text'>{t('banner.follow')}</h6>
        <ul>
          <li>
            <a href='https://www.instagram.com/GabrielMusica'>
              <img alt='Gabriel Instagram' className='img-fluid' src='/assets/images/music/icons/insta.png' />
            </a>
          </li>
          <li>
            <a href='https://www.instagram.com/nanavico'>
              <img alt='Nana Instagram' className='img-fluid' src='/assets/images/music/icons/insta.png' />
            </a>
          </li>
          <li>
            <a href='https://www.twitch.tv/nanagabriel'>
              <img alt='Twich' className='img-fluid' src='/assets/images/music/icons/twitch.png' />
            </a>
          </li>
          <li>
            <a href='https://discord.com/invite/2SzPMSZ'>
              <img alt='Discord' className='img-fluid' src='/assets/images/music/icons/discord.png' />
            </a>
          </li>
        </ul>
      </div>
      {/* <Container className='music-container'>
				<Row>
					<Col md='10' className='offset-md-1'>
						<div className='play-bg d-flex'>
							<div className='song-text-container h-100'>
								<div className='d-flex h-100'>
									<div className='center-img'>
										<img alt='' className='img-fluid' src='/assets/images/music/icons/girl.png' />
									</div>
									<div className='song-text'>
										<h5 className='text-white song-head'>Latest Song</h5>
										<h6 className='text-white song-sub-head'>Zrial doj</h6>
									</div>
								</div>
							</div>
							<div className='play-setting m-auto'>
								<div
									aria-label='media player'
									className='jp-audio'
									id='jp_container_1'
									role='application'
								>
									<div className='jp-type-playlist'>
										<div className='jp-gui jp-interface p-0 d-flex'>
											<div className='jp-controls'>
												<button className='jp-play m-r-15' role='button' tabIndex='0' />
											</div>
											<a onClick={sidesection}>
												<i aria-hidden='true' className='fa fa-ellipsis-v' />
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</Col>
				</Row>
			</Container> */}
    </section>
  );
};

Banner.getInitialProps = async () => ({
  namespacesRequired: ['common']
});

export default withTranslation('common')(Banner);
