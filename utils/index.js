import axios from 'axios';
import moment from 'moment';

export const checkIfAlive = async () => {
	try {
		const response = await axios.get('http://worldtimeapi.org/api/timezone/America/New_York');

		if (response.status < 205) {
			const time = moment(response.data.datetime);
			const date = moment(response.data.datetime).format('YYYY-MM-DD');
			const startDate = moment(`${date} 21:00:00`);
			const endDate = moment(`${date} 22:30:00`);
			const day = moment(response.data.datetime).format('dddd');
			if ((day === 'Saturday' || day === 'Friday') && time.isBetween(startDate, endDate)) {
				return new Promise((resolve, reject) => {
					resolve({ alive: true });
				});
			} else {
				return new Promise((resolve, reject) => {
					resolve({ alive: false });
				});
			}
		}

		return new Promise((resolve, reject) => {
			reject({ alive: false });
		});
	} catch (error) {
		return new Promise((resolve, reject) => {
			reject({ alive: false });
		});
	}
};
