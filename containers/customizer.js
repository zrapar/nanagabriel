import React, { useState } from 'react';
import { i18n, withTranslation } from '../i18n';

const Customizer = ({ t }) => {
	const [ divName, setDivName ] = useState(t('language'));

	const ChangeLang = async (divName) => {
		if (divName === 'es') {
			await i18n.changeLanguage('es');
			setDivName('en');
		} else {
			await i18n.changeLanguage('en');
			setDivName('es');
		}
	};

	return (
		<div className='theme-pannel-main'>
			<ul>
				<li id='lang_btn'>
					<a
						href={null}
						className='btn setting_btn'
						style={{ width: 35 }}
						onClick={() => ChangeLang(divName)}
					>
						<img src={`/assets/images/${divName}-flag.png`} width='35px' />
					</a>
				</li>
			</ul>
		</div>
	);
};

Customizer.getInitialProps = async () => ({
	namespacesRequired : [ 'common' ]
});

export default withTranslation('common')(Customizer);
