import React, { useState } from 'react';
import Nav from './nav';
import Link from 'next/link';
import { Container, Row, Col } from 'reactstrap';
import { withTranslation } from '../../i18n';

const Header = ({ t, ...props }) => {
	const [ show, setShow ] = useState(false);
	const [ sidebar, setSidebar ] = useState(false);
	const [ search, setSearch ] = useState(false);

	const clickSidebar = () => {
		setSidebar(!sidebar);
		document.querySelector('.navbar').classList.add('openSidebar');
	};

	return (
		<header className={`${props.className || 'app2'} loding-header nav-abs custom-scroll  `}>
			<Container>
				<Row>
					<Col>
						<nav>
							<a className='m-r-auto' href='/'>
								<img
									alt='logo'
									className='logo-img img-fluid'
									src='/assets/images/logo/logo.png'
									width='90%'
								/>
							</a>
							{/* <div className='responsive-btn'>
								<a className='toggle-nav' onClick={clickSidebar}>
									<i aria-hidden='true' className='fa fa-bars text-white' />
								</a>
							</div> */}
							{/* <Nav /> */}
							{props.shop && (
								<div className='top-header-right'>
									<ul>
										{/* <li className='search'>
											<a href='#' onClick={() => setSearch(!search)}>
												<i className='icon-search' />
											</a>
											<div
												aria-labelledby='dropdownMenuButton'
												className={`dropdown-menu ${search && `show`} dropdown-menu-right`}
											>
												<form className='form-inline search-form'>
													<div className='form-group'>
														<label className='sr-only'>Email</label>
														<input
															className='form-control-plaintext'
															placeholder='Search....'
															type='search'
														/>
														<span className='d-sm-none mobile-search' />
													</div>
												</form>
											</div>
										</li> */}

										<li className='account '>
											<a href='#' onClick={() => setShow(!show)}>
												<i className='icon-user' />
											</a>
											<div
												className={` bg-black dropdown-menu ${show &&
													`show`} dropdown-menu-right`}
											>
												<Link href='/'>
													<a href='#'>{t('header.login')}</a>
												</Link>
												<a href='#'>{t('header.account')}</a>
												<Link href='/'>
													<a href='#'>{t('header.wishlist')}</a>
												</Link>
												<Link href='/'>
													<a href='#'>{t('header.checkout')}</a>
												</Link>
											</div>
										</li>
									</ul>
								</div>
							)}
						</nav>
					</Col>
				</Row>
			</Container>
		</header>
	);
};

Header.getInitialProps = async () => ({
	namespacesRequired : [ 'common' ]
});

export default withTranslation('common')(Header);
